package com.realdolmen.air.util;


public enum UserRole {
    EMPLOYEE, PARTNER, CUSTOMER, GUEST
}
