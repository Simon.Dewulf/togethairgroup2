package com.realdolmen.air.util;

public class InputTester {

    public static boolean notNullOrEmptyStringArg(String arg) {
        return (arg != null && !arg.isEmpty());
    }

    public static boolean testEmailHasDomain(String email) {
        if (notNullOrEmptyStringArg(email)) {
            String[] strings = email.split("@");
            if (strings.length > 1) {
                return true;
            }
        }
        return false;
    }

}
