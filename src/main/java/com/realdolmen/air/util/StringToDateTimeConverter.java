package com.realdolmen.air.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@FacesConverter("realdolmen.dateTimeConverter")
public class StringToDateTimeConverter implements Converter {
    final static Logger logger = LoggerFactory.getLogger(StringToDateTimeConverter.class);

    @Override
    public LocalDateTime getAsObject(FacesContext facesContext, UIComponent uiComponent, String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyyv HH:mm");
        LocalDateTime localDateTime = null;
        try {
            localDateTime = LocalDateTime.parse(dateString, formatter);
        } catch (DateTimeParseException e) {
            logger.error("cannot parse date", e);
        }
        return localDateTime;
    }

    public LocalDateTime getAsObject(String dateString) {
        return getAsObject(null, null, dateString);
    }


    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        LocalDateTime localDateTime = (LocalDateTime) o;
        return localDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
    }

    public String getAsString(String returnDate) {
        return getAsString(null, null, returnDate);
    }
}