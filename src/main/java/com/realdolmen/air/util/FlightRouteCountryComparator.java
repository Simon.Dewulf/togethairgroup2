package com.realdolmen.air.util;

import com.realdolmen.air.datamodel.flightmodel.FlightRoute;

import java.util.Comparator;

public class FlightRouteCountryComparator implements Comparator<FlightRoute> {
    @Override
    public int compare(FlightRoute o1, FlightRoute o2) {
        int c = o1.getOrigin().getCountry().compareTo(o2.getOrigin().getCountry());
        if(c == 0){
            c = o1.getDestination().getCountry().compareTo(o2.getDestination().getCountry());
            if(c == 0){
                c = new FlightRouteNameComparator().compare(o1, o2);
            }
        }
        return c;
    }
}
