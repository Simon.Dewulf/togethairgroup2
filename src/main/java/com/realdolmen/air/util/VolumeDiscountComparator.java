package com.realdolmen.air.util;

import com.realdolmen.air.datamodel.VolumeDiscount;

import java.util.Comparator;

public class VolumeDiscountComparator implements Comparator<VolumeDiscount> {
    @Override
    public int compare(VolumeDiscount v1, VolumeDiscount v2) {
        return Integer.valueOf(v1.getNbOfSeatForThisDicscount()).compareTo(Integer.valueOf(v2.getNbOfSeatForThisDicscount()));
    }
}
