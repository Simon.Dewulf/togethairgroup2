package com.realdolmen.air.util;

import com.realdolmen.air.datamodel.flightmodel.FlightRoute;

import java.util.Comparator;

public class FlightRouteNameComparator implements Comparator<FlightRoute> {
    @Override
    public int compare(FlightRoute o1, FlightRoute o2) {
        int c = o1.getOrigin().getName().compareTo(o2.getOrigin().getName());
        if(c == 0){
            c = o1.getDestination().getName().compareTo(o2.getDestination().getName());
            if(c==0){
                c = new FlightRouteCountryComparator().compare(o1, o2);
            }
        }
        return c;
    }
}
