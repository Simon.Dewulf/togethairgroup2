package com.realdolmen.air.util;


public enum AirlineLuxuryClassEnum {
    ECONOMY, BUSINESS, FIRST_CLASS;

    private static AirlineLuxuryClassEnum[] vals = values();
    public AirlineLuxuryClassEnum next()
    {
        return vals[(this.ordinal()+1) % vals.length];
    }
}
