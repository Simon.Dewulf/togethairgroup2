package com.realdolmen.air.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@FacesConverter("realdolmen.dateConverter")
public class StringToDateConverter implements Converter {
    final static Logger logger = LoggerFactory.getLogger(StringToDateConverter.class);

    @Override
    public LocalDate getAsObject(FacesContext facesContext, UIComponent uiComponent, String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("DD/MM/YYYY");
        LocalDate localDate = null;
        try {
            localDate = LocalDate.parse(dateString, formatter);
        } catch (DateTimeParseException e) {
            logger.error("cannot parse date", e);
        }
        return localDate;
    }

    public LocalDate getAsObject(String dateString) {
        return getAsObject(null, null, dateString);
    }


    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o == null || o.equals("")) return null;
        LocalDate date = (LocalDate) o;
        return date.format(DateTimeFormatter.ofPattern("DD/MM/YYYY"));
    }

    public String getAsString(Object returnDate) {
        return getAsString(null, null, returnDate);
    }
}