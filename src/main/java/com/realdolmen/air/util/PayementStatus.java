package com.realdolmen.air.util;

public enum PayementStatus {
    Paid,
    Pending
}
