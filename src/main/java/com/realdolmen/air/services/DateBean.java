package com.realdolmen.air.services;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by KPRBL94 on 13/11/2018.
 */

@Named
@RequestScoped
public class DateBean implements Serializable {

    public LocalDate today(){
      return  LocalDate.now();
    }
}
