package com.realdolmen.air.services;


import com.realdolmen.air.datamodel.LuxuryWithCapacityAndPrices;
import com.realdolmen.air.datamodel.flightmodel.FlightInstance;
import com.realdolmen.air.util.AirlineLuxuryClassEnum;
import com.realdolmen.air.util.StringToDateConverter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RequestScoped
@Named
public class FlightSearchService implements Serializable {


    @Inject
    private SearchResultBean searchResultBean;

    private String from;
    private String destination;
    private Date date;
    private Date returnDate;
    private List<AirlineLuxuryClassEnum> luxuryClassEnums;

    @PersistenceContext
    private EntityManager entityManager;

    private String aReturn;
    private String flightType = "single";
    private Boolean economyCheckBox;
    private Boolean businessCheckBox;
    private Boolean firstClassCheckbox;
    private Integer numberOfSeats = 1;


    public String processFormData() {

        processStartAndDestination();
        this.setLuxuryClassEnums();

      //  StringToDateConverter converter = new StringToDateConverter();
      //  LocalDate localDate = converter.getAsObject(date);
        LocalDate localdate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDateTime localstartDate = localdate.atTime(0, 0, 0);
        LocalDateTime localEndDate = localdate.atTime(23, 59, 59);
       LocalDate returnLocalDate = null;
        LocalDateTime returnLocalStartDate = null;
        LocalDateTime returnLocalEndDate = null;
        if (returnDate != null) {
            returnLocalDate = returnDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            returnLocalStartDate = returnLocalDate.atTime(0, 0, 0);
            returnLocalEndDate = returnLocalDate.atTime(23, 59, 59);
        }


        if (luxuryClassEnums.isEmpty()) {
            searchResultBean.setSingleSearchResults(searchFlights(from, destination, localstartDate, localEndDate, numberOfSeats));
            searchResultBean.setReturnSearchResults(null);
        } else {
            searchResultBean.setSingleSearchResults(searchFlights(from, destination, localstartDate, localEndDate, luxuryClassEnums, numberOfSeats));
            searchResultBean.setReturnSearchResults(null);
        }

        if (returnDate != null) {
            if (luxuryClassEnums.isEmpty()) {
                searchResultBean.setReturnSearchResults(searchFlights(destination, from, returnLocalStartDate, returnLocalEndDate, numberOfSeats));
            } else
                searchResultBean.setReturnSearchResults(searchFlights(destination, from, returnLocalStartDate, returnLocalEndDate, luxuryClassEnums, numberOfSeats));
        }
        searchResultBean.setNumberOfSeatsRequested(numberOfSeats);
        return "FlightList.xhtml?faces-redirect=true";
    }

    void processStartAndDestination() {
        if(this.from.length()>5 && this.destination.length() > 5) {
            this.from = from.substring(1, 5).trim();
            this.destination = destination.substring(1, 5).trim();
        }
    }

    private List<AirlineLuxuryClassEnum> setLuxuryClassEnums() {
        List<AirlineLuxuryClassEnum> classes = new ArrayList<>();



        if (economyCheckBox) {
            classes.add(AirlineLuxuryClassEnum.ECONOMY);
        }
        if (businessCheckBox) {
            classes.add(AirlineLuxuryClassEnum.BUSINESS);
        }
        if (firstClassCheckbox) {
            classes.add(AirlineLuxuryClassEnum.FIRST_CLASS);
        }
        this.luxuryClassEnums = classes;
        return classes;
    }

    public List<FlightInstance> searchFlights(String start, String destination, LocalDateTime dateStart, LocalDateTime dateEnd, Integer numberOfSeats) {

        return entityManager.createQuery("select f from FlightInstance f left JOIN f.luxuryCapacitiesAndPrices c    " +
                "WHERE lower(f.flightRoute.origin.code) like lower(:start)" +
                " and lower(f.flightRoute.destination.code) like lower(:destination)" +
                " and f.departureLocalDateTime between :dateStart and :dateEnd " +
                " and c.numberOfSeats > :numberOfSeats", FlightInstance.class)
                .setParameter("start", start)
                .setParameter("destination", destination)
                .setParameter("dateStart", dateStart)
                .setParameter("dateEnd", dateEnd)
                .setParameter("numberOfSeats", numberOfSeats)
                .getResultList();

    }

    public List<FlightInstance> searchFlights(String start, String destination, LocalDateTime dateStart, LocalDateTime dateEnd, List<AirlineLuxuryClassEnum> luxuryClasses, Integer numberOfSeats) {
        List<FlightInstance> resultList = entityManager.createQuery("select f from FlightInstance f left JOIN f.luxuryCapacitiesAndPrices c " +
                " WHERE lower(f.flightRoute.origin.code) like lower(:start)" +
                " and lower(f.flightRoute.destination.code) like lower(:destination)" +
                " and f.departureLocalDateTime between :dateStart and :dateEnd " +
                " and c.flightQuality in(:luxuryClasses) " +
                " and c.numberOfSeats > :numberOfSeats", FlightInstance.class)
                .setParameter("start", start)
                .setParameter("destination", destination)
                .setParameter("dateStart", dateStart)
                .setParameter("dateEnd", dateEnd)
                .setParameter("luxuryClasses", luxuryClasses)
                .setParameter("numberOfSeats", numberOfSeats)
                .getResultList();
        return resultList;
    }

    public String getFlightType() {
        return flightType;
    }

    public SearchResultBean getSearchResultBean() {
        return searchResultBean;
    }

    public void setFlightType(String flightType) {
        this.flightType = flightType;
    }

    public void setSearchResultBean(SearchResultBean searchResultBean) {
        this.searchResultBean = searchResultBean;

    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;

    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;

    }

    public void setEconomyCheckBox(Boolean economyCheckBox) {
        this.economyCheckBox = economyCheckBox;
    }

    public List<AirlineLuxuryClassEnum> getLuxuryClassEnums() {
        return luxuryClassEnums;
    }

    public Boolean getEconomyCheckBox() {
        return economyCheckBox;
    }

    public void setBusinessCheckBox(Boolean businessCheckBox) {
        this.businessCheckBox = businessCheckBox;
    }


    public Boolean getBusinessCheckBox() {
        return businessCheckBox;
    }

    public Integer getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public void setFirstClassCheckbox(Boolean firstClassCheckbox) {
        this.firstClassCheckbox = firstClassCheckbox;
    }

    public Boolean getFirstClassCheckbox() {
        return firstClassCheckbox;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public EntityManager setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
        return this.entityManager;
    }
}
