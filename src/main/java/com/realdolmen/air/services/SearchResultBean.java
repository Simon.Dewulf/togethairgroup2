package com.realdolmen.air.services;

import com.realdolmen.air.datamodel.LuxuryWithCapacityAndPrices;
import com.realdolmen.air.datamodel.flightmodel.FlightInstance;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


@SessionScoped
@Stateful
@Named
public class SearchResultBean implements Serializable {

    private List<FlightInstance> singleSearchResults;
    private List<FlightInstance> returnSearchResults;
    private boolean isToggled;
    private Integer numberOfSeatsRequested = 1;

    private FlightInstance chosenFlight;


    public FlightInstance getChosenFlight() {
        return chosenFlight;
    }

    public void setChosenFlight(FlightInstance chosenFlight) {
        this.chosenFlight = chosenFlight;

    }

    public List<FlightInstance> getReturnSearchResults() {
        return returnSearchResults;
    }

    public void setReturnSearchResults(List<FlightInstance> returnSearchResults) {
        this.returnSearchResults = returnSearchResults;

    }


    public List<FlightInstance> getSingleSearchResults() {
        return singleSearchResults;
    }

    public void setSingleSearchResults(List<FlightInstance> singleSearchResults) {
        this.singleSearchResults = singleSearchResults;
    }


    public String getDisplayPrices(FlightInstance flightInstance) {
        String returnString = "";

        for (int i = 0; i< flightInstance.getLuxuryCapacitiesAndPrices().size(); i++) {
            returnString += flightInstance.getLuxuryCapacitiesAndPrices().get(i).getLuxuryFlightQuality().toString() + " " + flightInstance.getLuxuryCapacitiesAndPrices().get(i).getAskingPrice();
            if((i+1 <flightInstance.getLuxuryCapacitiesAndPrices().size())) {
                returnString+= "\n";
            }
        }
        return returnString;
    }



    public String getDisplayDiscounts(FlightInstance flightInstance) {
        String returnString = "";

        for (int i = 0; i< flightInstance.getVolumeDiscounts().size(); i++) {
            returnString += "for " + flightInstance.getVolumeDiscounts().get(i).getNbOfSeatForThisDicscount() + ": " + flightInstance.getVolumeDiscounts().get(i).getPurcentage() + "%";
            if((i+1 <flightInstance.getVolumeDiscounts().size())) {
                returnString+= "\n";
            }
        }
        return returnString;
    }

    public Integer getNumberOfSeatsRequested() {
        return numberOfSeatsRequested;
    }

    public SearchResultBean setNumberOfSeatsRequested(Integer numberOfSeatsRequested) {
        this.numberOfSeatsRequested = numberOfSeatsRequested;
        return this;
    }

    public String reserve(FlightInstance flight){
        this.chosenFlight = new FlightInstance();
        this.chosenFlight = flight;
        return "flightdetails.xhtml?faces-redirect=true";
    }
}
