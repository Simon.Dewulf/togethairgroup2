package com.realdolmen.air.services;


import com.realdolmen.air.datamodel.accounts.Person;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Named
@RequestScoped
public class PersonService {

    @PersistenceContext
    EntityManager entityManager;


    public Person findPersonById(long id){
       return entityManager.find(Person.class, id);
    }













}
