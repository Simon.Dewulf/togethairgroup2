package com.realdolmen.air.services;

import com.realdolmen.air.beans.CurrentUser;
import com.realdolmen.air.datamodel.accounts.Person;
import com.realdolmen.air.util.InputTester;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named
@Stateful
@SessionScoped
public class LoginService implements Serializable {

    private Logger LOGGER = Logger.getLogger(LoginService.class.getName());

    private String login;
    private String password;

    @PersistenceContext
    EntityManager entityManager;

    @Inject
    CurrentUser currentUser;

    public String authenticateUsingSetCredentials() {
        LOGGER.info("login value: \t" + this.login);
        LOGGER.info("password value: \t" + this.password);
        if(! InputTester.notNullOrEmptyStringArg(this.login)){return "ThankYou.xhtml?faces-redirect=true";}
        if(! InputTester.notNullOrEmptyStringArg(this.password)){return "ThankYou.xhtml?faces-redirect=true";}
        boolean b ;
        if (InputTester.testEmailHasDomain(this.login)) {
             b = this.loginWithEmail(this.login, password);

        } else {
            b = (this.loginWithUserName(this.login, password));

        }
           if (b){
            return "index.xhtml?faces-redirect=true";
        }
          else {
              return "ThankYou.xhtml?faces-redirect=true";
           }
    }

    private boolean loginWithUserName(String username, String password) {
        Person user;
        LOGGER.log(Level.INFO, "Finding user by un for auth");
        Query query = entityManager.createNamedQuery("Person.findByUserName", Person.class);
        query.setParameter("username2find", username);
        try {
            user = (Person) query.getResultList().get(0);
        } catch (Exception e) {
            LOGGER.info("no user object was found");
            return false;
        }
        return this.authenticateUser(user, password);
    }

    private boolean loginWithEmail(String username, String password) {
        Person user;
        LOGGER.log(Level.INFO,"Finding user by email for auth");
        Query query = entityManager.createNamedQuery("Person.findByEmail", Person.class);
        query.setParameter("email2find", username);
        try {

            List<Person> personList = query.getResultList();
            user = personList.get(0);
        } catch (Exception e) {
            LOGGER.info(e.toString());
            return false;
        }
        return this.authenticateUser(user, password);
    }

    private boolean authenticateUser(Person user, String password) {
        if (user == null) {
            LOGGER.info("Authenication failed, invalid user");
            return false;
        }
        try {
            if (user.authenticate(password)) {
                this.currentUser.setCurrentUser(user);
                LOGGER.info("Authenication succeeded, valid user");
                boolean loggedIn = this.currentUser.isLoggedIn();
                String tmp = Boolean.toString(loggedIn);
                LOGGER.info("Logged in: \t " + tmp);
                return true;
            }
        } catch (NoSuchAlgorithmException e) {
            LOGGER.info("Authenication failed, server side error");
            return false;
        }
        LOGGER.info("Authenication failed, invalid password");
        return false;
    }

    public String logout(){
        this.currentUser.setCurrentUser(null);
        LOGGER.info("logout performed");
        return "index.xhtml?faces-redirect=true";
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
