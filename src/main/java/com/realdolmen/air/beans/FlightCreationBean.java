package com.realdolmen.air.beans;

import com.realdolmen.air.datamodel.LuxuryWithCapacityAndPrices;
import com.realdolmen.air.datamodel.VolumeDiscount;
import com.realdolmen.air.datamodel.accounts.Person;
import com.realdolmen.air.datamodel.flightmodel.Airline;
import com.realdolmen.air.datamodel.flightmodel.FlightInstance;
import com.realdolmen.air.services.PersonService;
import com.realdolmen.air.util.AirlineLuxuryClassEnum;
import com.realdolmen.air.util.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@ViewScoped
@Stateful
@Named
public class FlightCreationBean implements Serializable {
    final Logger logger = LoggerFactory.getLogger(FlightCreationBean.class.getName());

    @PersistenceContext
    EntityManager entityManager;

    @Inject
    CurrentUser currentUser;
    @Inject
    PersonService personService;
    @Inject
    AirlineSearchBean airlineSearchBean;
    @Inject
    FlightSearchBean flightSearchBean;

    /*
    Needed to set params for creation
     */
    private FlightInstance flight = new FlightInstance();
    private VolumeDiscount volumeDiscount = new VolumeDiscount();
    private VolumeDiscount volumeDiscount2 = new VolumeDiscount();
    private LuxuryWithCapacityAndPrices luxuryWithCapacityAndPrices = new LuxuryWithCapacityAndPrices();
    private LuxuryWithCapacityAndPrices luxuryWithCapacityAndPrices1 = new LuxuryWithCapacityAndPrices();

    private LocalDate repeatTill = LocalDate.now();

    private boolean repeat = false;
    private boolean repeatweekly = false;
    int intvalue;
    private String floatValue;


    /*
    needed for creation logic
     */
    private AirlineLuxuryClassEnum airlineLuxuryClassEnum = AirlineLuxuryClassEnum.ECONOMY;


    /*
    needed for display functionality
     */
    private boolean add = false;
    private boolean edit = false;
    private boolean detailsLuxury = false;
    private boolean detailsDiscounts = false;

    private boolean editLuxury = false;

    private long id = 0l;



/*
Edit functionality
 */

    @PostConstruct
    private void init() {
    }

    public boolean createFlight() {
        if (!this.currentUser.getCurrentUserRole().equals(UserRole.PARTNER)) return false;
        this.flight.setAirline(this.getAirline());

        try {
            entityManager.persist(flight);
            this.id = flight.getId();
        } catch (Exception e) {
            logger.warn("could not persist new flight");
            return false;
        }
        //this.deactivateViewBooleans();
        return true;
    }

    private Airline getAirline() {
        return this.airlineSearchBean.getAirlineByID(personService.findPersonById(this.currentUser.getCurrentUserId()).getAirlineId());
    }

    public boolean addSetVolumeDiscount() {
        if (Double.compare(this.volumeDiscount.getPurcentage(), 0.0d) == 0 || this.volumeDiscount.getNbOfSeatForThisDicscount() == 0) {
            return false;
        }
        this.flight.addVolumeDiscount(volumeDiscount);
        return true;
    }

    public void deleteDiscount(VolumeDiscount discount){
        entityManager.remove(entityManager.find(VolumeDiscount.class, discount.getId()));
    }

    public void updateLuxuryClass(long luxuryID, float value){
        float local = value;
        if(Float.compare(0.0F, local) == 0) return;
        try {
            LuxuryWithCapacityAndPrices lux = entityManager.find(LuxuryWithCapacityAndPrices.class, luxuryID);
            lux.setAskingPrice(local);
            entityManager.merge(lux);
        }catch (Exception e){
            logger.info("Failed to update price for luxury " + luxuryID);
        }
    }

    /*
    Display functionality
     */

    public List<FlightInstance> getAllFlights() {
        List<FlightInstance> list = this.flightSearchBean.getAllFlights();
        if (!this.currentUser.getCurrentUserRole().equals(UserRole.PARTNER)) return list;
        List<FlightInstance> result = new ArrayList<>();
        for (FlightInstance flight : list) {
            if (this.canEditFlight(flight.getId())) {
                result.add(flight);
            }
        }
        return result;
    }

    public boolean canEditFlight(long flightID) {
        long userID = this.currentUser.getCurrentUserId();
        if (userID == 0L) return false;
        try {
            Person user = this.personService.findPersonById(userID);
            FlightInstance flight = this.flightSearchBean.getFlightById(flightID);
            return flight.getAirline().getId() == user.getAirlineId();
        } catch (Exception e) {//
            // logger.info("failed to find entities by id");
        }
        return false;
    }

    public String getNextLuxury() {
        this.airlineLuxuryClassEnum = this.airlineLuxuryClassEnum.next();
        return this.airlineLuxuryClassEnum.name();
    }

    public boolean saveSetLuxury() {
        if (Float.compare(this.luxuryWithCapacityAndPrices.getBasePrice(), 0.0F) == 0 ||
                this.luxuryWithCapacityAndPrices.getNumberOfSeats() == 0) {
            return false;
        }
        this.flight.setCapacityAndBasePriceForLuxury(this.luxuryWithCapacityAndPrices);
        return true;
    }

    public void viewLuxuryDetails(long id) {
        this.setId(id);
        this.deactivateViewBooleans();
        this.detailsLuxury = true;
    }

    public void viewDiscountDetails(long id) {
        this.setId(id);
        this.deactivateViewBooleans();
        this.detailsDiscounts = true;
    }

    public void viewEditFlight() {
        this.setId(id);
        this.deactivateViewBooleans();
        this.edit = true;
    }

    public void viewAddFlight() {
        this.deactivateViewBooleans();
        this.add = true;
    }


    private void deactivateViewBooleans() {
        this.add = false;
        this.edit = false;
        this.detailsLuxury = false;
        this.detailsDiscounts = false;
    }

    private void toggleEditLuxury(){
        this.editLuxury = ! this.editLuxury;
    }


    public HashSet<LuxuryWithCapacityAndPrices> searchLuxuryDetails() {
        try {
            return new HashSet<LuxuryWithCapacityAndPrices>(flightSearchBean.getFlightById(this.id)
                    .getLuxuryCapacitiesAndPrices());
        } catch (Exception e) {
            return new HashSet<>();
        }
    }

    public HashSet<VolumeDiscount> searchDiscountDetails() {
        try {
            return new HashSet<VolumeDiscount>(flightSearchBean.getFlightById(this.id).getVolumeDiscounts());
        } catch (Exception e) {
            return new HashSet<>();
        }
    }


    /*
    getters and setters for attributes
     */
    public FlightInstance getFlight() {
        return flight;
    }

    public void setFlight(FlightInstance flight) {
        this.flight = flight;
    }

    public VolumeDiscount getVolumeDiscount() {
        return volumeDiscount;
    }

    public void setVolumeDiscount(VolumeDiscount volumeDiscount) {
        this.volumeDiscount = volumeDiscount;
    }

    public LuxuryWithCapacityAndPrices getLuxuryWithCapacityAndPrices() {
        return luxuryWithCapacityAndPrices;
    }

    public void setLuxuryWithCapacityAndPrices(LuxuryWithCapacityAndPrices luxuryWithCapacityAndPrices) {
        this.luxuryWithCapacityAndPrices = luxuryWithCapacityAndPrices;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isDetailsLuxury() {
        return detailsLuxury;
    }

    public void setDetailsLuxury(boolean detailsLuxury) {
        this.detailsLuxury = detailsLuxury;
    }

    public boolean isDetailsDiscounts() {
        return detailsDiscounts;
    }

    public void setDetailsDiscounts(boolean detailsDiscounts) {
        this.detailsDiscounts = detailsDiscounts;
    }

    public int getIntvalue() {
        return intvalue;
    }

    public void setIntvalue(int intvalue) {
        this.intvalue = intvalue;
    }

    public boolean isEditLuxury() {
        return editLuxury;
    }

    public void setEditLuxury(boolean editLuxury) {
        this.editLuxury = editLuxury;
    }

    public String getFloatValue() {
        return floatValue;
    }

    public void setFloatValue(String flaot) {

        this.floatValue = flaot;
    }

    public LuxuryWithCapacityAndPrices getLuxuryWithCapacityAndPrices1() {
        return luxuryWithCapacityAndPrices1;
    }

    public void setLuxuryWithCapacityAndPrices1(LuxuryWithCapacityAndPrices luxuryWithCapacityAndPrices1) {
        this.luxuryWithCapacityAndPrices1 = luxuryWithCapacityAndPrices1;
    }

    public LocalDate getRepeatTill() {
        return repeatTill;
    }

    public void setRepeatTill(LocalDate repeatTill) {
        this.repeatTill = repeatTill;
    }

    public VolumeDiscount getVolumeDiscount2() {
        return volumeDiscount2;
    }

    public void setVolumeDiscount2(VolumeDiscount volumeDiscount2) {
        this.volumeDiscount2 = volumeDiscount2;
    }

    public boolean isRepeatweekly() {
        return repeatweekly;
    }

    public void setRepeatweekly(boolean repeatweekly) {
        this.repeatweekly = repeatweekly;
    }
}
