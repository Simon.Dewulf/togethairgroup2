package com.realdolmen.air.beans;

import com.realdolmen.air.datamodel.flightmodel.Airport;
import com.realdolmen.air.datamodel.flightmodel.FlightRoute;
import com.realdolmen.air.util.FlightRouteCountryComparator;
import com.realdolmen.air.util.FlightRouteNameComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateful;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@Stateful
@ViewScoped
public class RouteSearchBean implements Serializable {
    final Logger logger = LoggerFactory.getLogger(RouteSearchBean.class);

    private boolean defaultOrdering = true;
    private String from;
    private String to;

    @PersistenceContext
    EntityManager entityManager;

    @Inject
    AirportSearchBean airportSearchBean;

    public List<FlightRoute> getAllRoutesOrderedByCountry(){
        Query q = this.entityManager.createQuery("select r from FlightRoute r", FlightRoute.class);
        ArrayList<FlightRoute> list = new ArrayList<>();
        list.addAll(q.getResultList());
        list.sort(new FlightRouteCountryComparator());
        return list;
    }

    public List<FlightRoute> getAllRoutesOrderedByName(){
        Query q = this.entityManager.createQuery("select r from FlightRoute r", FlightRoute.class);
        ArrayList<FlightRoute> list = new ArrayList<>();
        list.addAll(q.getResultList());
        list.sort(new FlightRouteNameComparator());
        if(defaultOrdering){
            list.sort(new FlightRouteCountryComparator());
        } else {
            list.sort(new FlightRouteNameComparator());
        }
        return list;
    }


    /*
    Search by codes, names or countries from airports.
     */
    public List<FlightRoute> getRoutesBySetStringParams(){
        List<Airport> from = this.findAirport(this.from);
        List<Airport> to = this.findAirport(this.to);
        List<FlightRoute> flightRouteList = new ArrayList<>();
        Query query = this.entityManager.createQuery("SELECT r from FlightRoute r where r.origin = ?1 " +
                "and r.destination = ?2",FlightRoute.class);
        for(Airport aFrom: from){
            for(Airport aTo: to){
                query.setParameter(1, aFrom);
                query.setParameter(2, aTo);
                flightRouteList.addAll(query.getResultList());
            }
        }
        return flightRouteList;
    }

    private List<Airport> findAirport(String arg){
        if(arg == null || arg.isEmpty()) return airportSearchBean.getAllAirports();
        Query query = this.entityManager.createQuery("select a from Airport a where a.code like ?1", Airport.class);
        query.setParameter(1, '%'+arg+'%');
        List<Airport> result = query.getResultList();
        if(! result.isEmpty()){
            return result;
        }

        query = this.entityManager.createQuery("select a from Airport a where a.name like ?1", Airport.class);
        query.setParameter(1, '%'+arg+'%');
        result = query.getResultList();
        if(! result.isEmpty()){
            return result;
        }

        query = this.entityManager.createQuery("select a from Airport a where a.country like ?1", Airport.class);
        query.setParameter(1, '%'+arg+'%');
        result = query.getResultList();
        return result;
    }


    public boolean isDefaultOrdering() {
        return defaultOrdering;
    }

    public void setDefaultOrdering(boolean defaultOrdering) {
        this.defaultOrdering = defaultOrdering;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
