package com.realdolmen.air.beans;

import com.realdolmen.air.datamodel.flightmodel.Airport;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Named
@RequestScoped
public class AirportSearchBean implements Serializable {

    @PersistenceContext
    EntityManager entityManager;

//    private Airport autoCompleteMe;

    public List<Airport> getAllAirports(){
        try{return entityManager.createQuery("select a from Airport a", Airport.class).getResultList();}
        catch (Exception e) {return new ArrayList<>();}
    }

    public Airport getAirportById(long id){
        try{
            return entityManager.createQuery("select a from Airport a WHERE  a.id = ?1", Airport.class)
                    .setParameter(1, id)
                    .getSingleResult();
        } catch (Exception e){
            return null;
        }
    }

    public List<String> getAllCountries(){
        List<String> countriesList = entityManager.createQuery("select distinct a.country from Airport a ",String.class).getResultList();
        Collections.sort(countriesList);
        return countriesList;
    }

    public List<Integer> getAllTimeZones(){
        List<Integer> timeZones = new ArrayList<>();
        for (int i = -12; i <= 12; i++){
            timeZones.add(i);
        }
        return timeZones;
    }

//    public List<Airport> autocompleteAirports(String autoCompleteMe){
//        List<Airport> autocompletions = this.getAllAirports();
//        List<Airport> autocompletionsResult = new ArrayList<>();
//
//        for(Airport airport: autocompletions){
//            if (airport.getCountry().toLowerCase().contains(autoCompleteMe) ||
//                    airport.getName().toLowerCase().contains(autoCompleteMe) ||
//                    airport.getCode().toLowerCase().contains(autoCompleteMe)){
//                autocompletionsResult.add(airport);
//            }
//        }
//        return autocompletionsResult;
//    }
//
//    public Airport getAutoCompleteMe() {
//        return autoCompleteMe;
//    }
//
//    public void setAutoCompleteMe(Airport autoCompleteMe) {
//        this.autoCompleteMe = autoCompleteMe;
//    }
}
