package com.realdolmen.air.beans;

import com.realdolmen.air.datamodel.flightmodel.Airline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@RequestScoped
@Named
public class AirlineSearchBean {
    private final Logger LOGGER = LoggerFactory.getLogger(AirlineSearchBean.class);

    @PersistenceContext
    EntityManager em;

    public List<Airline> getAllAirlines(){
        try { return em.createQuery("select a from Airline a", Airline.class).getResultList(); }
        catch (Exception e){ return new ArrayList<>(); }
    }

    public Airline getAirlineByID(long airlineId) {
        try {return em.find(Airline.class, airlineId);}
        catch (Exception e){LOGGER.info("nO airport found for id" + airlineId); return null;}
    }
}
