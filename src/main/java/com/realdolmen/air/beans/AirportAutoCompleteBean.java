package com.realdolmen.air.beans;

import com.realdolmen.air.datamodel.flightmodel.Airport;

import javax.ejb.Stateful;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@Stateful
@ViewScoped
public class AirportAutoCompleteBean implements Serializable{

    @Inject
    AirportSearchBean airportSearchBean;

    private Airport foundAirport;

    private String airportResult;


    public List<String> autocompleteAirports(String autoCompleteMe) {
        List<Airport> autocompletions = this.airportSearchBean.getAllAirports();
        List<String> autocompletionsResult = new ArrayList<>();
        autoCompleteMe = autoCompleteMe.toLowerCase();

        for (Airport airport : autocompletions) {
            if (airport.getCountry().toLowerCase().contains(autoCompleteMe) ||
                    airport.getName().toLowerCase().contains(autoCompleteMe) ||
                    airport.getCode().toLowerCase().contains(autoCompleteMe)) {
                autocompletionsResult.add(airport.printDetails());

            }
        }

        return autocompletionsResult;
    }

    public Airport getFoundAirport() {
        return foundAirport;
    }

    public void setFoundAirport(Airport autoCompleteMe) {
        this.foundAirport = autoCompleteMe;
    }

    public String getAirportResult() {
        return airportResult;
    }

    public void setAirportResult(String airportResult) {
        this.airportResult = airportResult;
    }

    public AirportAutoCompleteBean setAirportSearchBean(AirportSearchBean airportSearchBean) {
        this.airportSearchBean = airportSearchBean;
        return this;
    }


}
