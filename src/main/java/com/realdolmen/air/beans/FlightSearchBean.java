package com.realdolmen.air.beans;

import com.realdolmen.air.datamodel.flightmodel.FlightInstance;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Named
@RequestScoped
public class FlightSearchBean {

    @PersistenceContext
    EntityManager entityManager;

    public List<FlightInstance> getAllFlights() {
        Query q = this.entityManager.createQuery("select f from FlightInstance f", FlightInstance.class);
        return q.getResultList();
    }

    public FlightInstance getFlightById(long id) {
        return this.entityManager.find(FlightInstance.class, id);
    }
}
