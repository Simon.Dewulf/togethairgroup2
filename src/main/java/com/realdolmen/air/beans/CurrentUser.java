package com.realdolmen.air.beans;

import com.realdolmen.air.datamodel.accounts.Person;
import com.realdolmen.air.util.UserRole;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@Stateful
@SessionScoped
public class CurrentUser implements Serializable {

    private Person currentUser;

    public void setCurrentUser(Person person) {
        this.currentUser = person;
    }

    public boolean isLoggedIn() {
        return this.currentUser != null;
    }

    public UserRole getCurrentUserRole() {
        if (this.isLoggedIn()) {

            return this.currentUser.getRole();
        }
        return UserRole.PARTNER;
    }

    public long getCurrentUserId() {
        if (this.isLoggedIn()) {
            return this.currentUser.getId();
        }
        return 0;
    }

    public long setCurrentByUserId() {
        if (this.isLoggedIn()) {
            return this.currentUser.getId();
        }
        return 0;
    }

    public Person getCurrentUser() {
        return currentUser;
    }
}