package com.realdolmen.air.beans;

import com.realdolmen.air.datamodel.accounts.Person;
import com.realdolmen.air.exceptions.InvalidPasswordException;
import com.realdolmen.air.exceptions.InvalidStringArgException;
import com.realdolmen.air.services.LoginService;
import com.realdolmen.air.util.UserRole;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.faces.bean.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.logging.Logger;

@Named
@Stateful
@ApplicationScoped
//@Singleton
public class Main implements Serializable {

    @PersistenceContext
    private EntityManager em;
    private Logger LOGGER = Logger.getLogger(LoginService.class.getName());
    private boolean madeUsers = false;

    private byte[] makeHashPassword(String passstring) {
        byte[] password = null;
        try {
            password = MessageDigest.getInstance("SHA-256").digest(passstring.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return password;
    }

    private Person makePerson(String username, String first, String lastname, String passstring, UserRole role) throws Exception, InvalidStringArgException, InvalidPasswordException {
        Person person = new Person();
        Random r = new Random();
        int RandomInt = r.nextInt(1000);
        String randomIntString = Integer.toString(RandomInt);
        username = username + randomIntString;
        String email = first + randomIntString +"@hotmail.com";
        person.setEmail(email);
        person.setFirstName(first);
        person.setLastName(lastname);
        person.setUserName(username);
        byte[] password = makeHashPassword(passstring);
        person.setPassword(password);
        person.setRole(role);
        return person;
    }
    @PostConstruct
    public void setupPersons() throws Exception, InvalidStringArgException, InvalidPasswordException {
        try {


            if (!madeUsers) {
                Person traveller = makePerson("leeroy", "Leeroy", "Jenkins", "123", UserRole.CUSTOMER);
                Person worker = makePerson("bob","bob","debouwer","123",UserRole.EMPLOYEE);
                Person airline = makePerson("ryanair","ryanair","ryanair","123",UserRole.PARTNER);
                em.persist(traveller);
                em.persist(worker);
                em.persist(airline);
                LOGGER.warning("Person has been persisted");
                toggleMadeUsers();
            }
        } catch (Exception  e){

        }

    }

    private void toggleMadeUsers(){
        this.madeUsers = true;
    }

}
