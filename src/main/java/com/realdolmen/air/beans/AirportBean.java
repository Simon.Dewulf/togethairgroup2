package com.realdolmen.air.beans;

import com.realdolmen.air.datamodel.flightmodel.Airport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateful;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AUABH09 on 12-Nov-18.
 */
@Named
@Stateful
@ViewScoped
public class AirportBean implements Serializable {

    @PersistenceContext
    private EntityManager em;

    final Logger logger = LoggerFactory.getLogger(AirportBean.class.getName());

    private Airport airport = new Airport();

    private boolean edit = false;


    private boolean add = false;

    public void save(){
        try{
            em.persist(this.airport);
            add = false;
            reset();
        } catch (Exception e) {
            logger.warn("could not persist new airport");
        }
        reset();
    }

    public void update() {
        edit = false;
        try {
            em.merge(airport);
            reset();
        } catch (Exception e) {
            logger.warn("could not update this airport");
        }
    }

    public void reset() {
        this.airport = new Airport();
    }

//    public Airport findAirport(String code) {
//        Query query = em.createQuery("SELECT a from Airport a where a.code = :code", Airport.class);
//        return query.getSingleResult()!= null ? airport = (Airport) query.getSingleResult() : null;
//    }

    public void editAirport(long id) {
        edit = true;
        add = false;
        Query query = em.createQuery("select a from Airport a WHERE a.id = :id",Airport.class);
        query.setParameter("id",id);
        airport = (Airport) query.getSingleResult();
    }
    public void addAirport(){
        add = true;
        edit = false;
        reset();
    }


    public List<String> getAllCountries() {
        List<String> countriesList = em.createQuery("select distinct a.country from Airport a ",String.class).getResultList();

        return countriesList;
    }

    public List<Integer> getAllTimeZones() {
        List<Integer> timeZones = new ArrayList<>();
        for (int i = -11; i < 12; i++){
            timeZones.add(i);
        }
        return timeZones;
    }

    public Airport getAirport() {
        return airport;
    }

    public void setAirport(Airport airport) {
        this.airport = airport;
    }

    public boolean isEdit() {
        return edit;
    }
    public boolean isAdd() {
        return add;
    }
}
