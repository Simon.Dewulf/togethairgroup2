package com.realdolmen.air.datamodel;

import com.realdolmen.air.datamodel.accounts.Person;
import com.realdolmen.air.datamodel.flightmodel.FlightInstance;
import com.realdolmen.air.util.PayementStatus;

import javax.persistence.*;

/**
 * Created by AUABH09 on 08-Nov-18.
 */
@Entity
public class Booking {
    @GeneratedValue
    @Id
    private long id;

    @ManyToOne
    private FlightInstance flightInstance;

    @ManyToOne
    private Person customer;

    @Enumerated(EnumType.STRING)
    private PayementStatus payementStatus;
    private int nbSeats;
    private float totalPrice;

//    public setBookingParams(FlightInstance flightInstance, Person customer, PayementStatus payementStatus, int nbSeats, float totalPrice) {
//        this.flightInstance = flightInstance;
//        this.customer = customer;
//        this.payementStatus = payementStatus;
//        this.nbSeats = nbSeats;
//        this.totalPrice = totalPrice;
//    }

    public long getId() {
        return id;
    }

    public FlightInstance getFlightInstance() {
        return flightInstance;
    }

    public void setFlightInstance(FlightInstance flightInstance) {
        this.flightInstance = flightInstance;
    }

    public Person getCustomer() {
        return customer;
    }

    public void setCustomer(Person customer) {
        this.customer = customer;
    }

    public PayementStatus getPayementStatus() {
        return payementStatus;
    }

    public void setPayementStatus(PayementStatus payementStatus) {
        this.payementStatus = payementStatus;
    }

    public int getNbSeats() {
        return nbSeats;
    }

    public void setNbSeats(int nbSeats) {
        this.nbSeats = nbSeats;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(this == obj) return true;
        if (obj instanceof Booking){
           Booking toEqual = (Booking) obj;
           if (!this.getPayementStatus().equals(toEqual.getPayementStatus())){
               return false;
           }
            if (! this.getCustomer().equals(toEqual.getCustomer())){
                return false;
            }
            if (! this.getFlightInstance().equals(toEqual.getFlightInstance())){
                return false;
            }
            if (this.getNbSeats() != (toEqual.getNbSeats())){
                return false;
            }
            if (this.getTotalPrice() != (toEqual.getTotalPrice())){
                return false;
            }
           return true;
        } return false;
    }
}
