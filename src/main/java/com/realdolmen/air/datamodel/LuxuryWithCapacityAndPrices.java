package com.realdolmen.air.datamodel;

import com.realdolmen.air.exceptions.InvalidCapacityException;
import com.realdolmen.air.util.AirlineLuxuryClassEnum;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by AUABH09 on 08-Nov-18.
 */
@Entity
public class LuxuryWithCapacityAndPrices {

    @GeneratedValue
    @Id
    private long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AirlineLuxuryClassEnum flightQuality;
    private int numberOfSeats;
    @Column(nullable = false)
    private Float basePrice = 0f;
    private Float askingPrice;


    @Transient
    public final static double ASKING_PROFIT_MARGIN = 0.05f;

    public long getId() {
        return id;
    }

    public AirlineLuxuryClassEnum getFlightQuality() {
        return flightQuality;
    }

    public void setFlightQuality(AirlineLuxuryClassEnum flightQuality) {
        this.flightQuality = flightQuality;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int nbOfSeat) {
        if(this.numberOfSeats != 0) this.numberOfSeats = nbOfSeat;
    }

    public void decreaseNbOfSeats(int decreaseNbOfSeat) throws InvalidCapacityException {
        int newTotal = this.numberOfSeats - decreaseNbOfSeat;
        if (newTotal < 0) {
            throw new InvalidCapacityException();
        }
        this.numberOfSeats = newTotal;
    }

    public float getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(float basePrice) {
        this.basePrice = new Float(basePrice);
    }

    public float getAskingPrice() {
        if (this.askingPrice == null) {
            return new Float(this.basePrice * (1f + LuxuryWithCapacityAndPrices.ASKING_PROFIT_MARGIN));
        }
        return this.askingPrice.floatValue();
    }

    public void setAskingPrice(float askingPrice) {
        this.askingPrice = new Float(askingPrice);
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        if (this == o) return true;
        if (!(o instanceof LuxuryWithCapacityAndPrices)) return false;
        LuxuryWithCapacityAndPrices that = (LuxuryWithCapacityAndPrices) o;
        return getNumberOfSeats() == that.getNumberOfSeats() &&
                flightQuality == that.flightQuality &&
                Objects.equals(getBasePrice(), that.getBasePrice()) &&
                Objects.equals(getAskingPrice(), that.getAskingPrice());
    }
}
