package com.realdolmen.air.datamodel.accounts;

import com.realdolmen.air.datamodel.flightmodel.Airline;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value = "A")
public class Partner extends Person {
    @ManyToOne
    private Airline linkedAirline;

//    public Airline getLinkedAirline() {
//        return linkedAirline;
//    }

    public void setLinkedAirline(Airline linkedAirline) {
        if (linkedAirline == null) {
            throw new IllegalArgumentException();
        }
        this.linkedAirline = linkedAirline;
    }

    @Override
    public long getAirlineId() {
        return this.linkedAirline.getId();
    }
}
