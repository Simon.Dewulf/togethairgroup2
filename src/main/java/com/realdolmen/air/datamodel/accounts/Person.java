package com.realdolmen.air.datamodel.accounts;

import com.realdolmen.air.exceptions.InvalidPasswordException;
import com.realdolmen.air.exceptions.InvalidStringArgException;
import com.realdolmen.air.util.InputTester;
import com.realdolmen.air.util.UserRole;

import javax.persistence.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "partner_disc", discriminatorType = DiscriminatorType.CHAR)
@DiscriminatorValue("P")
@NamedQueries({
        @NamedQuery(name = "Person.findByEmail",
        query = "SELECT p from Person p Where p.email = :email2find"),
        @NamedQuery(name = "Person.findByUserName",
                query = "SELECT p from Person p Where p.userName = :username2find")
})
public class Person {
    @Id
    @GeneratedValue
    private long id;

    private String firstName;
    private String lastName;

    @Column(unique = true)
    private String userName;
    @Enumerated(EnumType.STRING)
    private UserRole role;
    @Column(unique = true)
    private String email;
    //hashed value!
    private byte[] password;

    public long getId() {
        return id;
    }

    public long getAirlineId() {
        return 0L;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) throws InvalidStringArgException {
        if (!InputTester.notNullOrEmptyStringArg(firstName)) {
            throw new InvalidStringArgException();
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) throws InvalidStringArgException {
        if (!InputTester.notNullOrEmptyStringArg(lastName)) {
            throw new InvalidStringArgException();
        }
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) throws InvalidStringArgException {
        if (!InputTester.notNullOrEmptyStringArg(userName)) {
            throw new InvalidStringArgException();
        }
        this.userName = userName;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) throws InvalidStringArgException {
        if (!InputTester.testEmailHasDomain(email)) {
            throw new InvalidStringArgException();
        }
        this.email = email;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) throws InvalidPasswordException {
        if (password == null) {
            throw new InvalidPasswordException();
        }
        if (password.length == 0) {
            throw new InvalidPasswordException();
        }
        this.password = password;
    }

    public boolean authenticate(String passwordToAuth) throws NoSuchAlgorithmException {
        if (passwordToAuth == null) {
            return false;
        }
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] bytepas = md.digest(passwordToAuth.getBytes());
        return Arrays.equals(password, bytepas);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(obj instanceof Person){
            if(this.getEmail() != null) {
                return this.getEmail().equals(((Person) obj).getUserName());
            }
            if(this.getUserName() != null){
                return this.getUserName().equals(((Person) obj).getUserName());
            }
        }
        return false;
    }
}
