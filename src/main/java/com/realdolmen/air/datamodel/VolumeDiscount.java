package com.realdolmen.air.datamodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by AUABH09 on 09-Nov-18.
 */
@Entity
public class VolumeDiscount {

    @GeneratedValue
    @Id
    private long id;

    private int nbOfSeatForThisDicscount;
    private double purcentage;

    public long getId() {
        return id;
    }

    public int getNbOfSeatForThisDicscount() {
        return nbOfSeatForThisDicscount;
    }

    public void setNbOfSeatForThisDicscount(int nbOfSeatForThisDicscount) {
        this.nbOfSeatForThisDicscount = nbOfSeatForThisDicscount;
    }

    public double getPurcentage() {
        return purcentage;
    }

    public void setPurcentage(double purcentage) {
        this.purcentage = purcentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VolumeDiscount)) return false;
        VolumeDiscount that = (VolumeDiscount) o;
        return getNbOfSeatForThisDicscount() == that.getNbOfSeatForThisDicscount() &&
                Double.compare(that.getPurcentage(), getPurcentage()) == 0;
    }
}
