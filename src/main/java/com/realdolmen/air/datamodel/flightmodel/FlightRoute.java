package com.realdolmen.air.datamodel.flightmodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
public class FlightRoute {
    @Id
    @GeneratedValue
    private long id;
    @ManyToOne
    private Airport origin;
    @ManyToOne
    private Airport destination;

    public FlightRoute() {
    }

    public long getId() {
        return id;
    }

    public Airport getOrigin() {
        return origin;
    }

    public Airport getDestination() {
        return destination;
    }

    public void setOrigin(Airport origin) {
        this.origin = origin;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("From " + this.origin.getName());
        stringBuilder.append(" in " + this.origin.getCountry());
        stringBuilder.append(" --> to " + this.getDestination().getName());
        stringBuilder.append(" in " + this.getDestination().getCountry());
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        if (this == o) return true;
        if (!(o instanceof FlightRoute)) return false;
        FlightRoute that = (FlightRoute) o;
        return Objects.equals(getOrigin(), that.getOrigin()) &&
                Objects.equals(getDestination(), that.getDestination());
    }
}
