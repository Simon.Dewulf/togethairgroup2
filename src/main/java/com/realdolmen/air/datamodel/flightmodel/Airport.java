package com.realdolmen.air.datamodel.flightmodel;

import com.realdolmen.air.exceptions.InvalidStringArgException;
import com.realdolmen.air.exceptions.InvalidTimeZoneException;
import com.realdolmen.air.util.InputTester;

import javax.persistence.*;

@Entity
public class Airport {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private long id;

    @Column(unique = true)
    private String code;

    private String name;
    private String country;
    private int timeZone;

    public long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) throws InvalidStringArgException {
        if (!InputTester.notNullOrEmptyStringArg(code)) {
            throw new InvalidStringArgException();
        }
        this.code = code;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", timeZone=" + timeZone +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(int timeZone) throws InvalidTimeZoneException {
        if (timeZone < -12 || timeZone > 12) {
            throw new InvalidTimeZoneException();
        }
        this.timeZone = timeZone;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }
        if(obj == null){
            return false;
        }
        if(obj instanceof Airport){
            Airport airport = (Airport) obj;
            return this.getCountry().equals(airport.getCode());
        }
        return false;
    }

    public String printDetails() {
        return "( " + this.getCode() + " ) " + this.getName() + " - " + this.getCountry();
    }
}
