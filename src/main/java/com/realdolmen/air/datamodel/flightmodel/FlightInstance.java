package com.realdolmen.air.datamodel.flightmodel;

import com.realdolmen.air.datamodel.LuxuryWithCapacityAndPrices;
import com.realdolmen.air.datamodel.VolumeDiscount;
import com.realdolmen.air.exceptions.InvalidCapacityException;
import com.realdolmen.air.exceptions.InvalidScheduleException;
import com.realdolmen.air.util.AirlineLuxuryClassEnum;
import com.realdolmen.air.util.LocalDateTimeAttributeConverter;
import com.realdolmen.air.util.VolumeDiscountComparator;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.TreeSet;

@Entity
public class FlightInstance {

    @GeneratedValue
    @Id
    private long id;

    @ManyToOne
    private FlightRoute flightRoute;

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime departureLocalDateTime;

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime arrivalLocalDateTime;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "FlightInstance_Id")
    private List<LuxuryWithCapacityAndPrices> luxuryCapacitiesAndPrices;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "FlightInstance_Id")
    private List<VolumeDiscount> volumeDiscounts;


    @ManyToOne
    private Airline airline;

//    public FlightInstance() {
//        for (AirlineLuxuryClassEnum alce : AirlineLuxuryClassEnum.values()) {
//            LuxuryWithCapacityAndPrices luxuryWithCapacityAndPrices = new LuxuryWithCapacityAndPrices();
//            luxuryWithCapacityAndPrices.setNbOfSeat(0);
//            luxuryWithCapacityAndPrices.setBasePrice(0.0f);
//            luxuryWithCapacityAndPrices.setFlightQuality(alce);
//            this.setCapacityAndBasePriceForLuxury(luxuryWithCapacityAndPrices);
//        }
//    }

    public long getId() {
        return id;
    }

    public FlightRoute getFlightRoute() {
        return flightRoute;
    }

    public void setFlightRoute(FlightRoute flightRoute) {
        this.flightRoute = flightRoute;
    }

    public LocalDateTime getDepartureLocalDateTime() {
        return departureLocalDateTime;
    }

    public void setDepartureLocalDateTime(LocalDateTime departureCalendar) {
        this.departureLocalDateTime = departureCalendar;
    }

    public LocalDateTime getArrivalLocalDateTime() {
        return arrivalLocalDateTime;
    }

    public void setArrivalLocalDateTime(LocalDateTime arrivalCalendar) {
        this.arrivalLocalDateTime = arrivalCalendar;
    }

    public List<LuxuryWithCapacityAndPrices> getLuxuryCapacitiesAndPrices() {
        return luxuryCapacitiesAndPrices;
    }

    public void setLuxuryCapacitiesAndPrices(List<LuxuryWithCapacityAndPrices> capacities) {
        for (LuxuryWithCapacityAndPrices cap : capacities) {
            this.setCapacityAndBasePriceForLuxury(cap);
        }
    }

    public List<VolumeDiscount> getVolumeDiscounts() {
        return volumeDiscounts;
    }

    public void setVolumeDiscounts(List<VolumeDiscount> volumeDiscounts) {
        this.volumeDiscounts = volumeDiscounts;
    }

    public void addVolumeDiscount(VolumeDiscount volumeDiscount) {
        Comparator<VolumeDiscount> volumeDiscountComparator = new VolumeDiscountComparator();
        for (VolumeDiscount vd : this.volumeDiscounts) {
            if (volumeDiscountComparator.compare(volumeDiscount, vd) == 0) {
                vd.setPurcentage(volumeDiscount.getPurcentage());
                return;
            }
        }
        this.volumeDiscounts.add(volumeDiscount);
        return;
    }

    //returns the duration in minutes
    public double getDuration() throws InvalidScheduleException {
        double total = 0.0d;

        Duration duration = Duration.between(this.getDepartureLocalDateTime(), this.getArrivalLocalDateTime());


        total += duration.toMinutes();

        int refactorZones = this.getFlightRoute().getOrigin().getTimeZone() - this.getFlightRoute().getDestination().getTimeZone();
        total += refactorZones * 60.0d;
        if (total < 1) {
            throw new InvalidScheduleException();
        }

        return total;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public float getTicketPriceWithDiscounts(AirlineLuxuryClassEnum luxuryClassEnum, int nbTickets) {
        double percentage = this.getPercentageDiscount(nbTickets);
        float askingPrice = this.getCapacityAndPricesForLuxury(luxuryClassEnum).getAskingPrice();
        askingPrice -= percentage * askingPrice;
        return askingPrice;
    }

    public double getPercentageDiscount(int nbTickets) {
        TreeSet<VolumeDiscount> treeVolumeDiscounts = new TreeSet<VolumeDiscount>(new VolumeDiscountComparator());
        treeVolumeDiscounts.addAll(this.getVolumeDiscounts());
        VolumeDiscount temp = new VolumeDiscount();
        temp.setNbOfSeatForThisDicscount(nbTickets);
        temp = treeVolumeDiscounts.floor(temp);
        return temp.getPurcentage();
    }

    public boolean reserveSeats(AirlineLuxuryClassEnum luxuryClassEnum, int nbTickets) {
        LuxuryWithCapacityAndPrices lwcap = this.getCapacityAndPricesForLuxury(luxuryClassEnum);
        try {
            lwcap.decreaseNbOfSeats(nbTickets);
        } catch (InvalidCapacityException e) {
            return false;
        }
        return true;
    }

    public LuxuryWithCapacityAndPrices getCapacityAndPricesForLuxury(AirlineLuxuryClassEnum luxuryClassEnum) throws IllegalStateException {
        for (LuxuryWithCapacityAndPrices luxuryWithCapacityAndPrices : this.getLuxuryCapacitiesAndPrices()) {
            if (luxuryWithCapacityAndPrices.getFlightQuality().equals(luxuryClassEnum)) {
                return luxuryWithCapacityAndPrices;
            }
        }
        throw new IllegalStateException("Something's fishy with the luxury");
    }

    public void setCapacityAndBasePriceForLuxury(LuxuryWithCapacityAndPrices luxuryWithCapacityAndPrices) {
        try{
            LuxuryWithCapacityAndPrices lwcap = this.getCapacityAndPricesForLuxury(luxuryWithCapacityAndPrices.getFlightQuality());
            this.luxuryCapacitiesAndPrices.remove(lwcap);
        } catch (IllegalStateException e){
        }
        this.luxuryCapacitiesAndPrices.add(luxuryWithCapacityAndPrices);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!(o instanceof FlightInstance)) return false;
        FlightInstance that = (FlightInstance) o;
        return Objects.equals(getFlightRoute(), that.getFlightRoute()) &&
                Objects.equals(getDepartureLocalDateTime(), that.getDepartureLocalDateTime()) &&
                Objects.equals(getArrivalLocalDateTime(), that.getArrivalLocalDateTime()) &&
                Objects.equals(getAirline(), that.getAirline());
    }
}
