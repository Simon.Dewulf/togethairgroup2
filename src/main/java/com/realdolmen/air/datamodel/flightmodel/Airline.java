package com.realdolmen.air.datamodel.flightmodel;

import com.realdolmen.air.exceptions.InvalidStringArgException;
import com.realdolmen.air.util.InputTester;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Airline {

    @GeneratedValue
    @Id
    private long id;
    private String name;
    private String code;


    public long getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws InvalidStringArgException {
        if (!InputTester.notNullOrEmptyStringArg(name)) {
            throw new InvalidStringArgException();
        }
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) throws InvalidStringArgException {
        if (!InputTester.notNullOrEmptyStringArg(code)) {
            throw new InvalidStringArgException();
        }
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        if (this == o) return true;
        if (!(o instanceof Airline)) return false;
        Airline airline = (Airline) o;
        return Objects.equals(getName(), airline.getName()) &&
                Objects.equals(getCode(), airline.getCode());
    }
}
