package com.realdolmen.air.datamodel.flightmodel;

import com.realdolmen.air.exceptions.InvalidStringArgException;
import com.realdolmen.air.exceptions.InvalidTimeZoneException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AirportTest {

    private Airport airport;

    @Before
    public void setup() throws InvalidStringArgException, InvalidTimeZoneException {
        airport = new Airport();
        airport.setCountry("Belgium");
        airport.setName("Zaventem");
        airport.setTimeZone(0);
        airport.setCode("uniqueCode");
    }


    @Test
    public void getName() {
        assertEquals("Zaventem", this.airport.getName());

    }

    @Test
    public void setName() {
        this.airport.setName("Deurne local airport");
        assertEquals("Deurne local airport", this.airport.getName());
    }

    @Test
    public void getCountry() {
        assertEquals("Belgium", this.airport.getCountry());

    }

    @Test
    public void setCountry() {
        this.airport.setCountry("Netherlands");
        assertEquals("Netherlands", this.airport.getCountry());
    }

    @Test
    public void getTimeZone() {
        assertEquals(0, this.airport.getTimeZone());
    }

    @Test
    public void setTimeZone() throws InvalidTimeZoneException {
        this.airport.setTimeZone(10);
        assertEquals(10, this.airport.getTimeZone());
    }


    @Test
    public void setValidBorderTimeZone() throws InvalidTimeZoneException {
        this.airport.setTimeZone(12);
        assertEquals(12, this.airport.getTimeZone());
        this.airport.setTimeZone(-12);
        assertEquals(-12, this.airport.getTimeZone());
    }

    @Test(expected = InvalidTimeZoneException.class)
    public void setInvalidNegativeTimeZone() throws InvalidTimeZoneException {
        this.airport.setTimeZone(-13);

    }

    @Test(expected = InvalidTimeZoneException.class)
    public void setInvalidPositiveTimeZone() throws InvalidTimeZoneException {
        this.airport.setTimeZone(13);
    }

    @Test
    public void getCode() {
        assertEquals("uniqueCode", airport.getCode());
    }

    @Test
    public void setCode() throws InvalidStringArgException {
        airport.setCode("2");
        assertEquals("2", airport.getCode());
    }

    @Test(expected = InvalidStringArgException.class)
    public void setInvalidCode() throws InvalidStringArgException {
        airport.setCode("");
    }
}