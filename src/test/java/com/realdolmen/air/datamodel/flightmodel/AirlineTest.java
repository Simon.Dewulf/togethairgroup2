package com.realdolmen.air.datamodel.flightmodel;

import com.realdolmen.air.exceptions.InvalidStringArgException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AirlineTest {

    private String name = "naam";
    private String code = "code";

    private Airline underTest;


    @Before
    public void setUp() throws Exception, InvalidStringArgException {
        underTest = new Airline();
        underTest.setCode(code);
        underTest.setName(name);
    }


    @Test
    public void getName() {
        assertEquals(name, underTest.getName());
    }

    @Test
    public void setName() throws InvalidStringArgException {
        String string = "naam2";
        underTest.setName(string);
        assertEquals(string, underTest.getName());
    }

    @Test(expected = InvalidStringArgException.class)
    public void cannotSetNullName() throws InvalidStringArgException {
        underTest.setName(null);
    }

    @Test(expected = InvalidStringArgException.class)
    public void cannotSetEmptyName() throws InvalidStringArgException {
        underTest.setName("");
    }

    @Test
    public void getCode() {
        assertEquals(code, underTest.getCode());
    }

    @Test
    public void setCode() throws InvalidStringArgException {
        String string = "code2";
        underTest.setCode(string);
        assertEquals(string, underTest.getCode());
    }

    @Test(expected = InvalidStringArgException.class)
    public void cannotSetNullCode() throws InvalidStringArgException {
        underTest.setCode(null);
    }

    @Test(expected = InvalidStringArgException.class)
    public void cannotSetEmptyCode() throws InvalidStringArgException {
        underTest.setCode("");
    }
}