package com.realdolmen.air.datamodel.flightmodel;

import com.realdolmen.air.datamodel.LuxuryWithCapacityAndPrices;
import com.realdolmen.air.datamodel.VolumeDiscount;
import com.realdolmen.air.exceptions.InvalidCapacityException;
import com.realdolmen.air.exceptions.InvalidScheduleException;
import com.realdolmen.air.util.AirlineLuxuryClassEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.*;

/**
 * Created by AUABH09 on 09-Nov-18.
 */
@RunWith(MockitoJUnitRunner.class)
public class FlightInstanceTest {


    private FlightInstance flightInstance;


    private LocalDateTime calendarD;

    private LocalDateTime calendarA;


    @Mock
    private FlightRoute flightRoute;
    @Mock
    private Airport origin;
    @Mock
    private Airport destination;

    private LocalDateTime reserveLocalDateTime;
    @Mock
    LuxuryWithCapacityAndPrices luxuryWithCapacityAndPrices;
    VolumeDiscount volumeDiscount = new VolumeDiscount();

    @Mock
    private Airline airline;

    @Before
    public void setUp() throws InvalidCapacityException {
        this.flightInstance = new FlightInstance();
        flightInstance.setFlightRoute(this.flightRoute);
        calendarD = LocalDateTime.of(2018, 11, 10, 10, 15);
        calendarA = LocalDateTime.of(2018, 11, 10, 12, 30);
        flightInstance.setArrivalLocalDateTime(calendarA);
        flightInstance.setDepartureLocalDateTime(calendarD);
        Mockito.when(this.flightRoute.getDestination()).thenReturn(this.destination);
        Mockito.when(this.flightRoute.getOrigin()).thenReturn(this.origin);
        Mockito.when(this.origin.getTimeZone()).thenReturn(0);
        Mockito.when(this.destination.getTimeZone()).thenReturn(0);
        flightInstance.setLuxuryCapacitiesAndPrices(Collections.singletonList(this.luxuryWithCapacityAndPrices));
        Mockito.when(luxuryWithCapacityAndPrices.getFlightQuality()).thenReturn(AirlineLuxuryClassEnum.BUSINESS);
        flightInstance.setVolumeDiscounts(Collections.singletonList(volumeDiscount));
        volumeDiscount.setNbOfSeatForThisDicscount(10);
        volumeDiscount.setPurcentage(0.1);
        Mockito.when(this.luxuryWithCapacityAndPrices.getAskingPrice()).thenReturn(100F);


        //Mockito.when(this.flightInstance.getCapacityAndPricesForLuxury(Mockito.any(AirlineLuxuryClassEnum.class))).thenReturn(this.luxuryWithCapacityAndPrices);
    }
//    @Test
//    public void getId() {
//        assertEquals(0L, flightInstance.getId());
//    }

    @Test
    public void getFlightRouteId() {
        Mockito.when(flightRoute.getId()).thenReturn(1L);
        this.flightInstance.setFlightRoute(flightRoute);
        assertEquals(1L, flightInstance.getFlightRoute().getId());
    }

    @Test
    public void getDurationSameTimeZone() throws InvalidScheduleException {
        assertEquals(135, flightInstance.getDuration(), 0.1d);
    }

    @Test
    public void getDurationPositiveIncrementTimeZone() throws InvalidScheduleException {
        Mockito.when(this.destination.getTimeZone()).thenReturn(2);
        assertEquals(15, flightInstance.getDuration(), 0.1d);
    }

    @Test
    public void getDurationNegativeIncrementTimeZone() throws InvalidScheduleException {
        Mockito.when(this.destination.getTimeZone()).thenReturn(-2);
        assertEquals(255, flightInstance.getDuration(), 0.1d);
    }

    @Test
    public void getDurationPositiveIncrementDay() throws InvalidScheduleException {
        calendarD = LocalDateTime.of(2018, 11, 10, 23, 15);
        flightInstance.setDepartureLocalDateTime(calendarD);
        calendarA = LocalDateTime.of(2018, 11, 11, 2, 30);
        flightInstance.setArrivalLocalDateTime(calendarA);
        System.out.println(calendarA.toString());
        assertEquals(195, flightInstance.getDuration(), 0.1d);
    }

    @Test
    public void getDurationNegativeIncrementDay() throws InvalidScheduleException {
        calendarD = LocalDateTime.of(2018, 11, 10, 3, 00);
        flightInstance.setDepartureLocalDateTime(calendarD);
        calendarA = LocalDateTime.of(2018, 11, 9, 23, 00);
        flightInstance.setArrivalLocalDateTime(calendarA);
        Mockito.when(this.destination.getTimeZone()).thenReturn(-5);
        assertEquals(60d, flightInstance.getDuration(), 0.1d);
    }

    @Test(expected = InvalidScheduleException.class)
    public void getInvalidDurationNegativeIncrementDay() throws InvalidScheduleException {
        calendarD = LocalDateTime.of(2018, 11, 10, 3, 00);
        flightInstance.setDepartureLocalDateTime(calendarD);
        calendarA = LocalDateTime.of(2018, 11, 9, 23, 00);
        flightInstance.setArrivalLocalDateTime(calendarA);
        assertEquals(60d, flightInstance.getDuration(), 0.1d);
    }

    @Test(expected = InvalidScheduleException.class)
    public void getInvalidDurationPositiveIncrementTimeZone() throws InvalidScheduleException {
        Mockito.when(this.destination.getTimeZone()).thenReturn(3);
        assertEquals(15, flightInstance.getDuration(), 0.1d);
    }

    @Test
    public void getInvalidDurationSameTimeZone() throws InvalidScheduleException {
        calendarA = LocalDateTime.of(2018, 11, 10, 3, 00);
        assertEquals(135, flightInstance.getDuration(), 0.1d);
    }

    @Test
    public void getInvalidDurationSameTimeZone2() throws InvalidScheduleException {
        calendarA = LocalDateTime.of(2018, 11, 9, 3, 00);
        assertEquals(135, flightInstance.getDuration(), 0.1d);
    }

    @Test
    public void getAirline() {
        Mockito.when(airline.getId()).thenReturn(1L);
        this.flightInstance.setAirline(airline);
        assertEquals(1l, this.flightInstance.getAirline().getId());
    }


    @Test
    public void getDepartureCalendar() {
        assertEquals(this.calendarD, this.flightInstance.getDepartureLocalDateTime());
    }

    @Test
    public void setDepartureCalendar() {
        this.flightInstance.setDepartureLocalDateTime(reserveLocalDateTime);
        assertEquals(this.reserveLocalDateTime, this.flightInstance.getDepartureLocalDateTime());

    }

    @Test
    public void getArrivalCalendar() {
        assertEquals(this.calendarA, this.flightInstance.getArrivalLocalDateTime());

    }

    @Test
    public void setArrivalCalendar() {
        this.flightInstance.setArrivalLocalDateTime(reserveLocalDateTime);
        assertEquals(this.reserveLocalDateTime, this.flightInstance.getArrivalLocalDateTime());

    }

    @Test
    public void getLuxuryCapacitiesAndPrices() {
        assertTrue(this.flightInstance.getLuxuryCapacitiesAndPrices().contains(luxuryWithCapacityAndPrices));
    }

    @Test
    public void getAndSetLuxuryCapacitiesAndPrices() {
        this.flightInstance.setLuxuryCapacitiesAndPrices(new ArrayList<LuxuryWithCapacityAndPrices>());
        assertTrue(this.flightInstance.getLuxuryCapacitiesAndPrices().isEmpty());
    }

    @Test
    public void getVolumeDiscounts() {
        assertTrue(this.flightInstance.getVolumeDiscounts().contains(volumeDiscount));

    }

    @Test
    public void setVolumeDiscounts() {
        this.flightInstance.setVolumeDiscounts(new ArrayList<VolumeDiscount>());
        assertTrue(this.flightInstance.getVolumeDiscounts().isEmpty());
    }

    @Test
    public void getTicketPriceWithDiscounts() {

        assertEquals(90, this.flightInstance.getTicketPriceWithDiscounts(AirlineLuxuryClassEnum.BUSINESS, 10), 0.1);

    }

    @Test
    public void getInvalidSeatReservation() throws InvalidCapacityException {
        Mockito.doThrow(InvalidCapacityException.class).when(luxuryWithCapacityAndPrices).decreaseNbOfSeats(Mockito.anyInt());
        assertFalse(this.flightInstance.reserveSeats(AirlineLuxuryClassEnum.BUSINESS, 100));
    }

    @Test
    public void reserveSeats() {
        assertTrue(this.flightInstance.reserveSeats(AirlineLuxuryClassEnum.BUSINESS, 12));
    }
}