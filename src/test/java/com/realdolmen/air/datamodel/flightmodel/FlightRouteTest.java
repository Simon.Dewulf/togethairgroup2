package com.realdolmen.air.datamodel.flightmodel;

import com.realdolmen.air.exceptions.InvalidScheduleException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class FlightRouteTest {

    private FlightRoute flightRoute;

    @Mock
    Airport from;
    @Mock
    Airport to;

    @Before
    public void setUp() throws Exception, InvalidScheduleException {
        this.flightRoute = new FlightRoute();
        this.flightRoute.setDestination(to);
        this.flightRoute.setOrigin(from);
    }

    @Test
    public void getOrigin() {
        assertEquals(this.from, this.flightRoute.getOrigin());
    }

    @Test
    public void getDestination() {
        assertEquals(this.to, this.flightRoute.getDestination());
    }
}