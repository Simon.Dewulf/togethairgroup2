package com.realdolmen.air.datamodel;

import com.realdolmen.air.datamodel.accounts.Person;
import com.realdolmen.air.datamodel.flightmodel.FlightInstance;
import com.realdolmen.air.util.PayementStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Created by AUABH09 on 08-Nov-18.
 */
@RunWith(MockitoJUnitRunner.class)
public class BookingTest {


    private Booking booking;

    @Mock
    private Person customer;
    @Mock
    private FlightInstance flightInstance;

    private PayementStatus payementStatus = PayementStatus.Paid;
    private int nbSeats = 2;
    private float totalPrice = 200.0f;


    @Before
    public void setUp() {
        this.booking = new Booking();
        this.booking.setCustomer(customer);
        this.booking.setFlightInstance(flightInstance);
        this.booking.setPayementStatus(payementStatus);
        this.booking.setNbSeats(nbSeats);
        this.booking.setTotalPrice(totalPrice);
    }

//    @Test
//    public void getId(){
//        // expected 0L,
//        assertEquals(0L,booking.getId());
//    }

    @Test
    public void getCustomerName() {
        Mockito.when(customer.getUserName()).thenReturn("John");
        this.booking.setCustomer(customer);
        assertEquals("John", this.booking.getCustomer().getUserName());
    }

    @Test
    public void getFlightInstanceId() {
        Mockito.when(flightInstance.getId()).thenReturn(1L);
        this.booking.setFlightInstance(flightInstance);
        assertEquals(1L, booking.getFlightInstance().getId());
    }

    @Test
    public void PayementStatusNotNullAndCanBeCompared() {
        this.booking.setPayementStatus(PayementStatus.Paid);
        assertNotNull(booking.getPayementStatus());
        assertEquals("Paid", booking.getPayementStatus().toString());
    }

    @Test
    public void NbOfSeatsCanBeSetAndGet() {
        this.booking.setNbSeats(10);
        assertEquals(10, booking.getNbSeats());
    }

    @Test
    public void TotalPriceCanBeSetAndGet() {
        this.booking.setTotalPrice(1000f);
        assertEquals(1000f, booking.getTotalPrice(), 0);
    }

    @Test
    public void testConstructorAndEquals() {
        Booking b2 = new Booking();
        b2.setCustomer(customer);
        b2.setFlightInstance(flightInstance);
        b2.setPayementStatus(payementStatus);
        b2.setNbSeats(2);
        b2.setTotalPrice(totalPrice);
        assertEquals(b2, this.booking);
    }


    @Test
    public void equals() {
        assertFalse(this.booking.equals(this.totalPrice));
        Booking b2 = null;
        assertFalse(this.booking.equals(b2));
        b2 = new Booking();
        assertFalse(this.booking.equals(b2));
        b2.setPayementStatus(payementStatus);
        assertFalse(this.booking.equals(b2));
        b2.setCustomer(customer);
        assertFalse(this.booking.equals(b2));
        b2.setFlightInstance(flightInstance);
        assertFalse(this.booking.equals(b2));
        b2.setNbSeats(nbSeats);
        assertFalse(this.booking.equals(b2));
        b2.setTotalPrice(totalPrice);
        assertEquals(b2, this.booking);
    }
}
