package com.realdolmen.air.datamodel;

import com.realdolmen.air.exceptions.InvalidCapacityException;
import com.realdolmen.air.util.AirlineLuxuryClassEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by AUABH09 on 09-Nov-18.
 */
@RunWith(MockitoJUnitRunner.class)
public class LuxuryWithCapacityAndPricesTest {

    private LuxuryWithCapacityAndPrices luxuryWithCapacityAndPrices;

    @Before
    public void setUp() {
        this.luxuryWithCapacityAndPrices = new LuxuryWithCapacityAndPrices();
    }

//    @Test
//    public void getId(){
//        assertEquals(0L, luxuryWithCapacityAndPrices.getId());
//    }

    @Test
    public void getFlightQuality() {
        luxuryWithCapacityAndPrices.setFlightQuality(AirlineLuxuryClassEnum.BUSINESS);
        assertEquals(AirlineLuxuryClassEnum.BUSINESS, luxuryWithCapacityAndPrices.getFlightQuality());

    }

    @Test
    public void getNbOfSeat() {
        luxuryWithCapacityAndPrices.setNumberOfSeats(100);
        assertEquals(100, luxuryWithCapacityAndPrices.getNumberOfSeats());
    }

    @Test
    public void getBasePrice() {
        luxuryWithCapacityAndPrices.setBasePrice(500f);
        assertEquals(500f, luxuryWithCapacityAndPrices.getBasePrice(), 0.1);
    }


    @Test
    public void getAskingPrice() throws Exception {
        luxuryWithCapacityAndPrices.setBasePrice(500f);
        assertEquals((LuxuryWithCapacityAndPrices.ASKING_PROFIT_MARGIN + 1.0) * this.luxuryWithCapacityAndPrices.getBasePrice(), luxuryWithCapacityAndPrices.getAskingPrice(), 0.1);
        luxuryWithCapacityAndPrices.setAskingPrice(525f);
        assertEquals(525f, luxuryWithCapacityAndPrices.getAskingPrice(), 0.1);
    }


    @Test(expected = InvalidCapacityException.class)
    public void decreaseNbOfSeats() throws InvalidCapacityException {
        this.luxuryWithCapacityAndPrices.setNumberOfSeats(10);
        this.luxuryWithCapacityAndPrices.decreaseNbOfSeats(11);
    }

    @Test
    public void decreaseMaxNbOfSeats() throws InvalidCapacityException {
        this.luxuryWithCapacityAndPrices.setNumberOfSeats(10);
        this.luxuryWithCapacityAndPrices.decreaseNbOfSeats(10);
        assertEquals(0, this.luxuryWithCapacityAndPrices.getNumberOfSeats());
    }

    @Test
    public void decreaseNormalNbOfSeats() throws InvalidCapacityException {
        this.luxuryWithCapacityAndPrices.setNumberOfSeats(10);
        this.luxuryWithCapacityAndPrices.decreaseNbOfSeats(6);
        assertEquals(4, this.luxuryWithCapacityAndPrices.getNumberOfSeats());
    }
}