package com.realdolmen.air.datamodel.accounts;

import com.realdolmen.air.exceptions.InvalidPasswordException;
import com.realdolmen.air.exceptions.InvalidStringArgException;
import com.realdolmen.air.util.UserRole;
import org.junit.Before;
import org.junit.Test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.*;

//TODO
public class PersonTest {

    private String first = "firstname";
    private String lastname = "lastname";
    private String username = "name4me";
    private String email = "name@domain.com";
    private String passstring = "paswoord123";
    private byte[] password;

    {
        try {
            password = MessageDigest.getInstance("SHA-256").digest(passstring.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private UserRole role = UserRole.CUSTOMER;

    private Person underTest;

    @Before
    public void setUp() throws Exception, InvalidStringArgException, InvalidPasswordException {
        underTest = new Person();
        underTest.setEmail(email);
        underTest.setFirstName(first);
        underTest.setLastName(lastname);
        underTest.setUserName(username);
        underTest.setPassword(password);
        underTest.setRole(role);
    }

    @Test
    public void getFirstName() {
        assertEquals(first, underTest.getFirstName());
    }

    @Test
    public void setFirstName() throws InvalidStringArgException {
        String string = "shhht";
        underTest.setFirstName(string);
        assertEquals(string, underTest.getFirstName());
    }

    @Test(expected = InvalidStringArgException.class)
    public void cannotSetEmptyFirstName() throws InvalidStringArgException {
        underTest.setFirstName("");
    }

    @Test(expected = InvalidStringArgException.class)
    public void cannotSetNullFirstName() throws InvalidStringArgException {
        underTest.setFirstName(null);
    }

    @Test
    public void getLastName() {
        assertEquals(lastname, underTest.getLastName());
    }

    @Test
    public void setLastName() throws InvalidStringArgException {
        String string = "shhht";
        underTest.setLastName(string);
        assertEquals(string, underTest.getLastName());
    }

    @Test(expected = InvalidStringArgException.class)
    public void cannotSetEmptyLastName() throws InvalidStringArgException {
        underTest.setLastName("");
    }

    @Test(expected = InvalidStringArgException.class)
    public void cannotSetNullLastName() throws InvalidStringArgException {
        underTest.setLastName(null);
    }

    @Test
    public void getUserName() {
        assertEquals(username, underTest.getUserName());
    }

    @Test
    public void setUserName() throws InvalidStringArgException {
        String string = "shhht";
        underTest.setUserName(string);
        assertEquals(string, underTest.getUserName());
    }

    @Test(expected = InvalidStringArgException.class)
    public void cannotSetEmptyUserName() throws InvalidStringArgException {
        underTest.setUserName("");
    }

    @Test(expected = InvalidStringArgException.class)
    public void cannotSetNullUserName() throws InvalidStringArgException {
        underTest.setUserName(null);
    }

    @Test
    public void getRole() {
        assertEquals(role, underTest.getRole());
    }

    @Test
    public void setRole() {
        underTest.setRole(UserRole.EMPLOYEE);
        assertEquals(UserRole.EMPLOYEE, underTest.getRole());
    }

    @Test
    public void getEmail() {
        assertEquals(email, underTest.getEmail());
    }

    @Test
    public void setEmail() throws InvalidStringArgException {
        String string = "mail@domain.com";
        underTest.setEmail(string);
        assertEquals(string, underTest.getEmail());
    }

    @Test(expected = InvalidStringArgException.class)
    public void setInvalidEmail() throws InvalidStringArgException {
        String string = "mail.domain.com";
        underTest.setEmail(string);
    }

    @Test
    public void getPassword() {
        assertEquals(password, underTest.getPassword());
    }

    @Test
    public void setPassword() throws InvalidPasswordException {
        byte[] pw2 = {12, 13, 45};
        underTest.setPassword(pw2);
        assertEquals(pw2, underTest.getPassword());
    }

    @Test(expected = InvalidPasswordException.class)
    public void setNullPassword() throws InvalidPasswordException {
        underTest.setPassword(null);
    }

    @Test(expected = InvalidPasswordException.class)
    public void setEmptyPassword() throws InvalidPasswordException {
        byte[] pw2 = {};
        underTest.setPassword(pw2);
    }

    @Test
    public void authenticate() throws NoSuchAlgorithmException {
        assertTrue(underTest.authenticate(this.passstring));
    }

    @Test
    public void authenticationFailsWithWrongPassword() throws NoSuchAlgorithmException {
        assertFalse(underTest.authenticate("i like bik codes"));
    }

    @Test
    public void authenticationFailsWithNullPassword() throws NoSuchAlgorithmException {
        assertFalse(underTest.authenticate(null));
    }

    @Test
    public void authenticationFailsWithEmptyPassword() throws NoSuchAlgorithmException {
        assertFalse(underTest.authenticate(""));
    }

    @Test
    public void getAirlineId() {
        assertEquals(0L, this.underTest.getAirlineId());
    }
}