package com.realdolmen.air.datamodel.accounts;

import com.realdolmen.air.datamodel.flightmodel.Airline;
import com.realdolmen.air.exceptions.InvalidStringArgException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class PartnerTest {


    private Partner partner;
    @Mock
    private Airline airline;
    @Mock
    private Airline airline2;


    @Before
    public void init() throws InvalidStringArgException {
        this.partner = new Partner();
        partner.setLinkedAirline(airline);
        partner.setUserName("bluh");
    }

    @Test
    public void setLinkedAirline() {
        Mockito.when(airline2.getId()).thenReturn(2L);
        this.partner.setLinkedAirline(airline2);
        assertEquals(2l, this.partner.getAirlineId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotSetLinkedAirlineToNull() {
        this.partner.setLinkedAirline(null);
    }

    @Test
    public void getAirlineId() {
        Mockito.when(airline.getId()).thenReturn(1L);
        assertEquals(1L, this.partner.getAirlineId());
    }

}