package com.realdolmen.air.util;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class InputTesterTest {


    @Test
    public void notNullOrEmptyStringArg() {
        assertTrue(InputTester.notNullOrEmptyStringArg("true"));
        assertFalse(InputTester.notNullOrEmptyStringArg(""));
        assertFalse(InputTester.notNullOrEmptyStringArg(null));

    }

    @Test
    public void testEmailHasDomain() {
        assertFalse(InputTester.testEmailHasDomain("nietwaar"));
        assertTrue(InputTester.testEmailHasDomain("wel@waar.be"));
    }
}