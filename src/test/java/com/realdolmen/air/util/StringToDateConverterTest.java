package com.realdolmen.air.util;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

/**
 * Created by KPRBL94 on 13/11/2018.
 */
public class StringToDateConverterTest {
    private StringToDateConverter stringToDateConverter = new StringToDateConverter();
    @Test
    public void getAsObject()  {
        LocalDate date = stringToDateConverter.getAsObject("02 03 1990");
        LocalDate from = LocalDate.of(1990,3,2);
        assertEquals(from, date);
    }

}