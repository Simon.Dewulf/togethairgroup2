package com.realdolmen.air.services;

import com.realdolmen.air.datamodel.flightmodel.FlightInstance;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class FlightSearchServiceTest extends AbstractPersistenceTest {


    private FlightSearchService flightSearchService = new FlightSearchService();

    @Before
    public void setUp() {
        flightSearchService.setEntityManager(em);
    }

    @Test
    public void searchFlights() {

        LocalDateTime departureStart = LocalDateTime.of(2018, 11, 29, 0, 0, 0);
        LocalDateTime departureEnd = LocalDateTime.of(2018, 11, 29, 23, 59, 59);


        List<FlightInstance> testlist = flightSearchService.searchFlights("Zanzibar", "Heathrow London Airport", departureStart, departureEnd, 5);
        System.out.println(testlist.size());

        assertFalse(testlist.isEmpty());
    }

    @Test
    public void processStartAndDestinationString() {
        flightSearchService.setDestination("( ZNZ )");
        flightSearchService.setFrom("( ZNZ )");
        flightSearchService.processStartAndDestination();
        assertEquals("ZNZ" ,flightSearchService.getDestination());
        assertEquals("ZNZ" ,flightSearchService.getFrom());
    }

    @Test
    public void searchFlights1() {
    }

    @Test
    public void searchFlights2() {
    }
}